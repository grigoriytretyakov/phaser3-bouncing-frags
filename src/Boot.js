import { settings } from './settings';


class Boot extends Phaser.Scene {
    constructor() {
        super({key: 'Boot'});
    }

    preload() {
        this.load.image(settings.wall, 'assets/wall.png');

        this.load.image(settings.bullet, 'assets/bullet.png');

        this.load.spritesheet(settings.frag, 'assets/frag.png', {
            frameWidth: settings.fragWidth,
            frameHeight: settings.fragHeight,
        });
        this.load.spritesheet(settings.player, 'assets/player.png', {
            frameWidth: settings.playerWidth,
            frameHeight: settings.playerHeight,
        });
    }

    create() {
        this.scene.start('Menu');
    }
}

export { Boot };
