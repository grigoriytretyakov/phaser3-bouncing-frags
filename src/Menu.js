import { settings } from './settings';


class Menu extends Phaser.Scene {
    constructor() {
        super({key: 'Menu'});
    }

    create() {
        let y = 50;
        this.add.sprite(settings.gameWidth / 2, 50, settings.bullet);
        this.add.sprite(settings.gameWidth / 2 - settings.playerWidth * 2, 50, settings.player, 4);
        this.add.sprite(settings.gameWidth / 2 + settings.fragWidth * 2, 50, settings.frag, 1);

        let yOffset = 100;
        this.startButton = this.add.sprite(
            settings.gameWidth / 2,
            settings.gameHeight / 2 + yOffset,
            settings.player,
            0);
        this.startButton.setInteractive()
            .on('pointerdown', () => {
                this.scene.start('Game');
            });

        this.input.on('pointermove', (pointer) => {
            this.pointerMove(pointer);
        });

        const fontStyle = {
            fontFamily: 'monospace',
            color: '#00e3ff',
            fontSize: 20,
        };

        let helpText = [
            'For moving use arrow keys, WAD or swipe on your touchscreen',
            'For shooting press SPACE or tap on your touchscreen',
            'Beware of zombies...',
        ].join('\n\n');
        this.add.text(100, settings.gameHeight / 2 - this.startButton.height * 2 - 100 + yOffset,
            helpText, fontStyle);

        let startText = '...and click on me to start';
        this.add.text(
            this.startButton.x,
            settings.gameHeight / 2 + this.startButton.height + 50 + yOffset,
            startText, fontStyle);
    }

    pointerMove(pointer) {
        let frame = pointer.position.x < this.startButton.x ? 0 : 3;

        let offset = 1;
        if (pointer.position.y > (this.startButton.y + this.startButton.height)) {
            offset = 2;
        }
        else if (pointer.position.y < (this.startButton.y - this.startButton.height)) {
            offset = 0;
        }

        frame += offset;

        this.startButton.setFrame(frame);
    }
}

export { Menu };
