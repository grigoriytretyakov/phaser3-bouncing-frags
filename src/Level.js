import { settings } from './settings';


const SCALE = 0.75;


class Level extends Phaser.Scene {
    constructor() {
        super({key: 'Game'});
    }

    create() {
        this.walls = null;
        this.player = null;
        this.frag = null;
        this.bullet = null;

        this.createWall();

        this.createPlayer();

        this.createFrag();

        this.createInput();

        this.cameras.main.on('camerafadeoutcomplete', () => {
            this.scene.start('Game');
        })
    }

    createPlayer() {
        this.player = this.physics.add.sprite(
            settings.playerStartX,
            settings.playerStartY,
            settings.player,
            1);
        this.player.setScale(SCALE);

        this.physics.add.collider(this.player, this.walls);

        this.player.setVelocityX(settings.playerVelocity);
    }

    createWall() {
        this.walls = this.physics.add.staticGroup();

        for (let x = settings.wallWidth / 2; x < settings.gameWidth; x += settings.wallWidth) {
            this.walls.create(x, settings.wallHeight / 2, settings.wall);
            this.walls.create(x, settings.gameHeight - settings.wallHeight / 2, settings.wall);
        }

        let maxY = settings.gameHeight - settings.wallHeight;
        for (let y = settings.wallHeight * 1.5; y < maxY; y += settings.wallHeight) {
            this.walls.create(settings.wallWidth / 2, y, settings.wall);
            this.walls.create(settings.gameWidth - settings.wallWidth / 2, y, settings.wall);
        }
    }

    createFrag() {
        let x = Phaser.Math.Between(
            settings.wallWidth + settings.fragWidth,
            settings.gameWidth - (settings.fragWidth + settings.wallWidth));
        let y = Phaser.Math.Between(
            settings.wallHeight + settings.fragHeight,
            settings.wallHeight + settings.fragHeight + settings.gameHeight / 2);

        this.frag = this.physics.add.sprite(x, y, settings.frag, 0);
        this.frag.setScale(SCALE);
        this.frag.setVelocityX(settings.fragVelocity);
        this.physics.add.collider(this.frag, this.walls, () => {
            if (this.frag.body.blocked.right) {
                this.frag.setFrame(1);
                this.frag.setVelocityX(-settings.fragVelocity);
            }
            else if (this.frag.body.blocked.left) {
                this.frag.setFrame(0);
                this.frag.setVelocityX(settings.fragVelocity);
            }
        });
    }

    createInput() {
        this.input.keyboard.on('keydown', this.handleKey, this);
        this.input.on("pointerup", this.handleSwipe, this);

    }

    handleKey(e) {
        switch (e.code) {
            case 'KeyA':
            case 'ArrowLeft':
                this.player.setVelocityX(-settings.playerVelocity);
                break;
            case 'KeyD':
            case 'ArrowRight':
                this.player.setVelocityX(settings.playerVelocity);
                break;
            case 'KeyW':
            case 'ArrowUp':
                if (this.player.body.blocked.down) {
                    this.player.setVelocityY(settings.playerJump);
                }
                break;
            case 'Space':
                this.fire();
                break;
        }
    }

    handleSwipe(e) {
        let swipeTime = e.upTime - e.downTime;
        let fastEnough = swipeTime < settings.swipeMaxTime;
        let swipe = new Phaser.Geom.Point(e.upX - e.downX, e.upY - e.downY);
        let swipeMagnitude = Phaser.Geom.Point.GetMagnitude(swipe);
        let longEnough = swipeMagnitude > settings.swipeMinDistance;
        if (longEnough && fastEnough) {
            Phaser.Geom.Point.SetMagnitude(swipe, 1);
            if (swipe.x > settings.swipeMinNormal) {
                this.player.setVelocityX(settings.playerVelocity);
            }
            if (swipe.x < -settings.swipeMinNormal) {
                this.player.setVelocityX(-settings.playerVelocity);
            }
            if (swipe.y < -settings.swipeMinNormal) {
                if (this.player.body.blocked.down) {
                    this.player.setVelocityY(settings.playerJump);
                }
            }
        }
        else {
            this.fire();
        }

    }

    fire() {
        if (!this.bullet) {
            let speed = settings.bulletVelocityX;
            let y = this.player.y;
            let x = this.player.x;
            let offset = this.player.width / 2 + 5;
            if (this.player.frame.name < 3) {
                x -= offset;
                speed = -speed;
            }
            else {
                x += offset;
            }

            if (x > settings.wallWidth && x < settings.gameWidth - settings.wallWidth) {
                this.bullet = this.physics.add.sprite(x, y, settings.bullet);
                this.bullet.setBounce(1);
                this.physics.add.collider(this.bullet, this.walls);
                this.bullet.setVelocity(speed, settings.bulletVelocityY);
            }
        }
    }

    update() {
        this.physics.world.collide(this.player, this.frag, () => {
            this.die();
        });

        let frame = this.player.body.velocity.x < 0 ? 0 : 3;
        if (this.player.body.velocity.y == 0) {
            frame += 1;
        }
        else if (this.player.body.velocity.y > 0) {
            frame += 2;
        }
        this.player.setFrame(frame);


        if (this.bullet) {
            const isOutside = this.bullet.x < 0
                || this.bullet.x > settings.gameWidth
                || this.bullet.y < 0
                || this.bullet.y > settings.gameHeight
            if (isOutside) {
                this.bullet.destroy();
                this.bullet = null;
            }
        }

        if (this.bullet) {
            this.physics.world.overlap(this.bullet, this.player, () => {
                this.bullet.destroy();
                this.bullet = null;
            });
        }
        if (this.bullet) {
            this.physics.world.overlap(this.bullet, this.frag, () => {
                this.frag.destroy();
                this.createFrag();
                this.bullet.destroy();
                this.bullet = null;
            });
        }

        if (this.bullet) {
            let distance = Phaser.Math.Distance.Between(
                this.frag.x, this.frag.y,
                this.bullet.x, this.bullet.y);

            if (distance < settings.fragWidth * 3 && this.frag.body.blocked.down) {
                this.frag.setVelocityY(settings.playerJump);
            }
        }
    }

    die() {
        this.player.setVelocityY(settings.deathVelocity);
        this.cameras.main.fade(settings.deathDuration, 50, 5, 5);
        this.cameras.main.shake(settings.deathDuration * 0.7);
    }

}

export { Level };
