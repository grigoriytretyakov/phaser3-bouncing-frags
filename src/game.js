import 'phaser';

import { settings } from './settings';
import { Boot } from './Boot';
import { Menu } from './Menu';
import { Level } from './Level';


function resizeGame() {
    let canvas = document.querySelector('canvas');
    let windowWidth = window.innerWidth;
    let windowHeight = window.innerHeight;
    if (windowWidth < game.config.width || windowHeight < game.config.height) {
        let windowRatio = windowWidth / windowHeight;
        let gameRatio = game.config.width / game.config.height;
        if (windowRatio < gameRatio) {
            canvas.style.width = windowWidth + "px";
            canvas.style.height = (windowWidth / gameRatio) + "px";
        }
        else {
            canvas.style.width = (windowHeight * gameRatio) + "px";
            canvas.style.height = windowHeight + "px";
        }
    }
    else {
        canvas.style.width = game.config.width + "px";
        canvas.style.height = game.config.height + "px";
    }
}


let game;

window.onload = () => {
    const gameConfig = {
        type: Phaser.AUTO,
        width: settings.gameWidth,
        height: settings.gameHeight,
        backgroundColor: '#00212b', //'#0b1728',

        physics: {
            default: 'arcade',
            arcade: {
                gravity: {
                    x: 0,
                    y: 1500
                },
                debug: false,
            }
        },

        scene: [
            Boot,
            Menu,
            Level,
        ]
    }

    game = new Phaser.Game(gameConfig);

    window.focus();
    resizeGame();
    window.addEventListener('resize', resizeGame);
}
