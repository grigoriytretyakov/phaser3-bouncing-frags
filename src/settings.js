const wall = 'wall';
const wallWidth = 20;
const wallHeight = 20;

const aspectRatio = 16 / 9;
const gameHeight = 600;
const gameWidth = Math.floor(gameHeight * aspectRatio / wallWidth) * wallWidth;

const frag = 'frag';
const fragWidth = 40;
const fragHeight = 40;
const fragVelocity = 450;

const player = 'player';
const playerUp = 'playerUp';
const playerDown = 'playerDown';
const playerWidth = 40;
const playerHeight = 40;
const playerStartX = 200;
const playerStartY = gameHeight - wallHeight - playerHeight / 2 - 50;
const playerVelocity = 400;
const playerJump = -600;

const bullet = 'bullet';
const bulletVelocityX = playerVelocity * 1.5;
const bulletVelocityY = playerJump * 0.6;

const deathVelocity = -2000;

const deathDuration = 1500;

const swipeMaxTime = 1200;
const swipeMinDistance = 30;
const swipeMinNormal = 0.85;

const settings = {
    gameWidth,
    gameHeight,

    wall,
    wallWidth,
    wallHeight,

    bullet,
    bulletVelocityX,
    bulletVelocityY,

    frag,
    fragWidth,
    fragHeight,
    fragVelocity,

    player,
    playerUp,
    playerDown,
    playerWidth,
    playerHeight,
    playerStartX,
    playerStartY,
    playerVelocity,
    playerJump,
    deathVelocity,
    deathDuration,
    aspectRatio,


    swipeMaxTime,
    swipeMinDistance,
    swipeMinNormal,
};


export { settings };
